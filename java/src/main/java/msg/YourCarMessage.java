package msg;

import track.CarID;

public class YourCarMessage extends Message<CarID>
{

	public YourCarMessage(CarID data, String gameId)
	{
		super(Messages.YOUR_CAR, data, gameId, null);
	}

}
