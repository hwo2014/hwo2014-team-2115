package msg;

public class PingMessage extends Message<Object>
{

	public PingMessage()
	{
		super(Messages.PING, null);
	}

}
