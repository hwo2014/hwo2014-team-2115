package msg;

public class GameStartMessage extends Message<Object>
{

	public GameStartMessage(String gameId, Integer gameTick)
	{
		super(Messages.GAME_START, null, gameId, gameTick);
	}

}
