package msg;

import com.google.gson.Gson;

public class Message<T>
{

	public final String msgType;
	public final T data;
	public final String gameId;
	public final Integer gameTick;

	public Message(final String msgType, final T data)
	{
		this(msgType, data, null, null);
	}
	
	public Message(final String msgType, final T data, String gameId, Integer gameTick)
	{
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
		this.gameTick = gameTick;
	}

	public String toJson()
	{
		return new Gson().toJson(this);
	
	}

	@Override
	public String toString()
	{
		return getClass().getSimpleName() + " [data=" +
				(data == null ? "null" : data.getClass() + "| " + data)  + ", gameId=" + gameId + ", gameTick=" + gameTick + "]";
	}
	
}
