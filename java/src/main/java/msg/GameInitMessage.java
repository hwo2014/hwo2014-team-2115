package msg;

import track.GameInit;

public class GameInitMessage extends Message<GameInit>
{
	
	public GameInitMessage(GameInit data)
	{
		super(Messages.GAME_INIT, data);
	}

}
