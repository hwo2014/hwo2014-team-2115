package msg.send;

import msg.Message;

public abstract class SendData
{

	public  abstract String messageType();
	
	public Message<SendData> makeMessage()
	{
		return new Message<SendData>(messageType(), this);
	}
}
