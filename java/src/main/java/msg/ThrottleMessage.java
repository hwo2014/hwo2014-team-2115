package msg;


public class ThrottleMessage extends Message<Double>
{

	public ThrottleMessage(double throttle)
	{
		super(Messages.THROTTLE, throttle);
	}
	
	public ThrottleMessage(double throttle, Integer gameTick)
	{
		super(Messages.THROTTLE, throttle, null, gameTick);
	}

}
