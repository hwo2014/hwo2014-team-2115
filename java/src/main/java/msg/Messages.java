package msg;

import com.google.gson.Gson;

public class Messages
{

	public final static String JOIN = "join";
	public final static String PING = "ping";
	public final static String YOUR_CAR = "yourCar";
	public final static String GAME_INIT = "gameInit";
	public final static String GAME_START = "gameStart";
	public final static String GAME_END = "gameEnd";
	public final static String CAR_POSITIONS = "carPositions";
	public final static String LAP_FINISHED = "lapFinished";
	public final static String FINISH = "finish";
	public final static String TOURNAMENT_END = "tournamentEnd";
	public final static String THROTTLE = "throttle";
	public final static String CRASH = "crash";
	public final static String SPAWN = "spawn";
	public final static String DID_NOT_FINISH = "dnf";
	public final static String SWITCH_LANE = "switchLane";

	
	
	public static void main(String[] args)
	{
		Gson gson = new Gson();
		
		final String kJoined = "{\"msgType\":\"join\",\"data\":{\"name\":\"PedalPower\",\"key\":\"CC0B1Gmb/TxyWA\"}}"; 
		JoinMessage joined = gson.fromJson(kJoined, JoinMessage.class);
		System.out.println(joined);
		System.out.println();
		
		
		final String kYourCar = "{\"msgType\":\"yourCar\",\"data\":{\"name\":\"PedalPower\",\"color\":\"red\"},\"gameId\":\"73fa83a4-c42a-454b-be37-66bbd43e0a93\"}";
		YourCarMessage your = gson.fromJson(kYourCar, YourCarMessage.class);
		System.out.println(your);
		System.out.println();
		
		final String kInit = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"PedalPower\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"73fa83a4-c42a-454b-be37-66bbd43e0a93\"}";
		GameInitMessage init = gson.fromJson(kInit, GameInitMessage.class);
		System.out.println(init);
		System.out.println();
		
		final String kStart = "{\"msgType\":\"gameStart\",\"data\":null,\"gameId\":\"73fa83a4-c42a-454b-be37-66bbd43e0a93\",\"gameTick\":0}";
		GameStartMessage start = gson.fromJson(kStart, GameStartMessage.class);
		System.out.println(start);
		System.out.println();
		
		final String kCarPos = "{\"msgType\":\"carPositions\",\"data\":[{\"id\":{\"name\":\"PedalPower\",\"color\":\"red\"},\"angle\":0.0,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":0.0,\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":0},\"lap\":0}}],\"gameId\":\"73fa83a4-c42a-454b-be37-66bbd43e0a93\",\"gameTick\":1}";
		CarPositionMessage pos = gson.fromJson(kCarPos, CarPositionMessage.class);
		System.out.println(pos);
		System.out.println();
			
		
	}
	
}
