package msg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import com.google.gson.Gson;

public class Client
{

	private String mHost;
	private int mPort;
	private String mBotName;
	private String mBotKey;

	Socket mSocket;
	PrintWriter mWriter;
	BufferedReader mReader;
	Thread mThread;

	private class ClientReader implements Runnable
	{

		@Override
		public void run()
		{
			String line = null;
			
			final String kLineStart = "{\"msgType\":\"";
			final String kCarPos = kLineStart + Messages.CAR_POSITIONS;
			final String kCrash = kLineStart + Messages.CRASH;
			final String kSpawn = kLineStart + Messages.SPAWN;
			
			Gson gson = new Gson();
			try {
				while ((line = mReader.readLine()) != null) {

					if (line.startsWith(kCarPos)) {
						CarPositionMessage msg = gson.fromJson(line, CarPositionMessage.class);
					}
					else if (line.startsWith(kCrash)) {
						
					}
					else if (line.startsWith(kCrash)) {
						
					}
					
					
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	public Client(String host, int port, String botName, String botKey) throws UnknownHostException, IOException
	{
		mHost = host;
		mPort = port;
		mBotName = botName;
		mBotKey = botKey;

		mSocket = new Socket(host, port);
		mWriter = new PrintWriter(new OutputStreamWriter(mSocket.getOutputStream(), "utf-8"));
		mReader = new BufferedReader(new InputStreamReader(mSocket.getInputStream(), "utf-8"));
	}

	public void join()
	{
		mThread = new Thread();

	}

	public void send(Message msg)
	{

	}

	public void close() throws IOException, InterruptedException
	{
		if (mThread != null) {
			if (mThread.isAlive()) mThread.interrupt();
			mThread.join();
			mThread = null;
		}

		if (mSocket != null) {
			mSocket.close();
			mSocket = null;
			mWriter = null;
			mReader = null;
		}
	}

}
