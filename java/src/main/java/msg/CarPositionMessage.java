package msg;

import java.util.ArrayList;

import track.Position;

public class CarPositionMessage extends Message<ArrayList<Position>>
{

	public CarPositionMessage(String msgType, ArrayList<Position> data, String gameId, Integer gameTick)
	{
		super(Messages.CAR_POSITIONS, data, gameId, gameTick);
	}

}
