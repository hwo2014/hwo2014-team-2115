package rvbot;

import java.util.ArrayList;

import msg.CarPositionMessage;
import msg.GameInitMessage;
import msg.GameStartMessage;
import msg.JoinMessage;
import msg.Message;
import msg.ThrottleMessage;
import msg.YourCarMessage;
import track.CarID;
import track.Position;

public class RVBot
{

	private enum State {
		INIT, JOINED, GAME_INITED, RACING, CRASHED, FINISHED
	};
	
	private State state;
	
	private CarID car;
	private Position pos;
	private double throttle = 0;
	
	public RVBot() {
		state = State.INIT;
	}
	
	public void joined(JoinMessage msg)
	{
		transition(State.JOINED);
	}
	
	public void yourCar(YourCarMessage msg)
	{
		car = msg.data;
	}
	
	public void updatePosition(CarPositionMessage msg) 
	{
		ArrayList<Position> positions = msg.data;
		for (Position pos: positions) {
			if (pos.getId().equals(car)) {
				this.pos = pos;
			}
		}
	}
	
	public Message nextMove()
	{
		if (pos == null) {
			return new ThrottleMessage(0.75);
		}
		
		double angle = pos.getAngle();
		if (Math.abs(pos.getAngle()) < 10) {
			throttle += 0.05;
		} else {
			throttle -= 0.1;
		}
		throttle = Math.max(Math.min(throttle, 0.75), 0);
		
		
		return new ThrottleMessage(throttle);
	}
	
	
	private void transition(State newState)
	{
		switch(state) {
		case INIT:
			if (newState != State.JOINED) {
				System.err.println("Transition state error: tried to go from " + state + " to " + newState);
			}
			else {
				state = newState;
			}
			break;
		case JOINED:
			if (newState != State.GAME_INITED) {
				System.err.println("Transition state error: tried to go from " + state + " to " + newState);
			}
			else {
				state = newState;
			}
			break;
		case GAME_INITED:
		case RACING:
		case CRASHED:
		case FINISHED:
			
		
		
		}
	}

	public void init(GameInitMessage init)
	{
		// TODO Auto-generated method stub
		
	}

	public void start(GameStartMessage start)
	{
		// TODO Auto-generated method stub
		
	}
	
//	public void end(GameEndMessage end)
//	{
//		
//	}
//	
//	public void tournamentEnd(TournamentEndMessage msg)
//	{
//		
//	}
	
	
	
	
	
}
