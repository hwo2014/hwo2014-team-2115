package rvbot;


public class MsgWrapper
{
	public final String msgType;
	public final Object data;

	MsgWrapper(final String msgType, final Object data)
	{
		this.msgType = msgType;
		this.data = data;
	}

	@Override
	public String toString()
	{
		if (data != null) {
			return msgType + "| " + data.getClass() + ": " + data;
		}
		else {
			return msgType + "| null";
		}
	}
}
