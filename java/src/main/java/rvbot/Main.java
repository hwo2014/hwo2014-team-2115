package rvbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

import msg.CarPositionMessage;
import msg.GameInitMessage;
import msg.GameStartMessage;
import msg.JoinMessage;
import msg.Message;
import msg.Messages;
import msg.PingMessage;
import msg.ThrottleMessage;
import msg.YourCarMessage;
import msg.send.SendData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import track.Bot;
import track.Race;
import ch.qos.logback.classic.BasicConfigurator;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class Main
{
	
	private static final Logger logger = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) throws UnsupportedEncodingException, IOException
	{
		
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		logger.info("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		Main main = new Main(writer, reader, botName, botKey);
		main.go();

		socket.close();

	}

	private final PrintWriter mWriter;
	private final BufferedReader mReader;
	private final RVBot mBot;
	private String mBotName;
	private String mBotKey;

	public Main(PrintWriter writer, BufferedReader reader, String botName, String botKey)
	{
		mWriter = writer;
		mReader = reader;
		mBot = new RVBot();
		mBotName = botName;
		mBotKey = botKey;
	}

	public void go() throws JsonSyntaxException, IOException
	{
		String line = null;

		send(new JoinMessage(new Bot(mBotName, mBotKey)));

		Gson gson = new Gson();
		
		while ((line = mReader.readLine()) != null) {
			
			logger.debug(line);
			line = line.trim();
			if (line.length() == 0) continue;
			final String firstBit = line.substring(0, line.indexOf(","));
			
			if (firstBit.contains(Messages.CAR_POSITIONS)) {
				CarPositionMessage msg = gson.fromJson(line, CarPositionMessage.class);
				mBot.updatePosition(msg);
			}
			else if (firstBit.contains(Messages.CRASH)) {
				logger.warn("Crash");
			}
			else if (firstBit.contains(Messages.SPAWN)) {
				
			}
			else if (firstBit.contains(Messages.LAP_FINISHED)) {
				logger.info("Lap");
			}
			else if (firstBit.contains(Messages.FINISH)) {
				
			}
			else if (firstBit.contains(Messages.DID_NOT_FINISH)) {
				
			}
			else if (firstBit.contains(Messages.JOIN)) {
				JoinMessage join = gson.fromJson(line, JoinMessage.class);
				mBot.joined(join);
				logger.info("Joined race: {}", join);
			}
			else if (firstBit.contains(Messages.YOUR_CAR)) {
				YourCarMessage myCar = gson.fromJson(line, YourCarMessage.class);
				mBot.yourCar(myCar);
			}
			else if (firstBit.contains(Messages.GAME_INIT)) {
				GameInitMessage init = gson.fromJson(line, GameInitMessage.class);
				mBot.init(init);
			}
			else if (firstBit.contains(Messages.GAME_START)) {
				GameStartMessage start = gson.fromJson(line, GameStartMessage.class);
				mBot.start(start);
			}
			else if (firstBit.contains(Messages.GAME_END)) {
				
			}
			else if (firstBit.contains(Messages.TOURNAMENT_END)) {
				
			}
			else {
				// unexpected message
				logger.warn("Enexpected message from server: {}", line);
			}
			
			Message<?> msg = mBot.nextMove();
			if (msg == null) {
				// ping the server
				msg = new PingMessage();
			}
			
			send(msg);
				
			
			
//			final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
//
//			if (msgFromServer.msgType.equals("carPositions")) {
//				send(new ThrottleMessage(0.5));
//				
//			}
//			else if (msgFromServer.msgType.equals("join")) {
//				System.out.println("Joined");
//			}
//			else if (msgFromServer.msgType.equals("gameInit")) {
//				System.out.println("Race init");
//				com.google.gson.internal.LinkedTreeMap map;
//				race = gson.fromJson(msgFromServer.data, Race.class);
//				System.out.println(race);
//			}
//			else if (msgFromServer.msgType.equals("gameEnd")) {
//				System.out.println("Race end");
//			}
//			else if (msgFromServer.msgType.equals("gameStart")) {
//				System.out.println("Race start");
//			}
//			else {
//				System.out.println(msgFromServer.msgType);
//				send(ping);
//			}

		}
	}

	public void send(Message<?> msg)
	{
		String json = msg.toJson();
		logger.debug("Sending: {}", json);
		mWriter.println(json);
		mWriter.flush();
	}

}
