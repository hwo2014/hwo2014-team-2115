package track;

public class Car
{
	private CarID id;

	private CarDimensions dimensions;

	public CarDimensions getDimensions()
	{
		return dimensions;
	}

	public CarID getId()
	{
		return id;
	}

	public void setDimensions(CarDimensions dimensions)
	{
		this.dimensions = dimensions;
	}

	public void setId(CarID id)
	{
		this.id = id;
	}

	@Override
	public String toString()
	{
		return "Car [id=" + id + ", dimensions=" + dimensions + "]";
	}

}
