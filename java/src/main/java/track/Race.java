package track;

import java.util.ArrayList;

public class Race
{

	private Track track;
	private ArrayList<Car> cars;
	private RaceSession raceSession;

	public ArrayList<Car> getCars()
	{
		return cars;
	}

	public RaceSession getRaceSession()
	{
		return raceSession;
	}

	public Track getTrack()
	{
		return track;
	}

	public void setCars(ArrayList<Car> cars)
	{
		this.cars = cars;
	}

	public void setRaceSession(RaceSession raceSession)
	{
		this.raceSession = raceSession;
	}

	public void setTrack(Track track)
	{
		this.track = track;
	}

	@Override
	public String toString()
	{
		return "Race [track=" + track + ", cars=" + cars + ", raceSession=" + raceSession + "]";
	}

}
