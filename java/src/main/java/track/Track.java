package track;

import java.util.ArrayList;

public class Track
{

	// public static void main(String[] args)
	// {
	//
	// String json =
	// "{ \"id\": \"keimola\", \"name\": \"Keimola\", \"pieces\": [ { \"length\": 100 }, { \"length\": 100 }, { \"length\": 100 }, { \"length\": 100, \"switch\": true }, { \"radius\": 100, \"angle\": 45 }, { \"radius\": 100, \"angle\": 45 }, { \"radius\": 100, \"angle\": 45 }, { \"radius\": 100, \"angle\": 45 }, { \"radius\": 200, \"angle\": 22.5, \"switch\": true }, { \"length\": 100 }, { \"length\": 100 }, { \"radius\": 200, \"angle\": -22.5 }, { \"length\": 100 }, { \"length\": 100, \"switch\": true }, { \"radius\": 100, \"angle\": -45 }, { \"radius\": 100, \"angle\": -45 }, { \"radius\": 100, \"angle\": -45 }, { \"radius\": 100, \"angle\": -45 }, { \"length\": 100, \"switch\": true }, { \"radius\": 100, \"angle\": 45 }, { \"radius\": 100, \"angle\": 45 }, { \"radius\": 100, \"angle\": 45 }, { \"radius\": 100, \"angle\": 45 }, { \"radius\": 200, \"angle\": 22.5 }, { \"radius\": 200, \"angle\": -22.5 }, { \"length\": 100, \"switch\": true }, { \"radius\": 100, \"angle\": 45 }, { \"radius\": 100, \"angle\": 45 }, { \"length\": 62 }, { \"radius\": 100, \"angle\": -45, \"switch\": true }, { \"radius\": 100, \"angle\": -45 }, { \"radius\": 100, \"angle\": 45 }, { \"radius\": 100, \"angle\": 45 }, { \"radius\": 100, \"angle\": 45 }, { \"radius\": 100, \"angle\": 45 }, { \"length\": 100, \"switch\": true }, { \"length\": 100 }, { \"length\": 100 }, { \"length\": 100 }, { \"length\": 90 } ], \"lanes\": [ { \"distanceFromCenter\": -10, \"index\": 0 }, { \"distanceFromCenter\": 10, \"index\": 1 } ], \"startingPoint\": { \"position\": { \"x\": -300, \"y\": -44 }, \"angle\": 90 } }";
	//
	// Gson gson = new Gson();
	//
	// Track data = gson.fromJson(json, Track.class);
	//
	// System.out.println(data);
	//
	// }

	private String id;
	private String name;
	private ArrayList<Piece> pieces;
	private ArrayList<Lane> lanes;

	private StartingPoint startingPoint;

	public String getId()
	{
		return id;
	}

	public ArrayList<Lane> getLanes()
	{
		return lanes;
	}

	public String getName()
	{
		return name;
	}

	public ArrayList<Piece> getPieces()
	{
		return pieces;
	}

	public StartingPoint getStartingPoint()
	{
		return startingPoint;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public void setLanes(ArrayList<Lane> lanes)
	{
		this.lanes = lanes;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setPieces(ArrayList<Piece> pieces)
	{
		this.pieces = pieces;
	}

	public void setStartingPoint(StartingPoint startingPoint)
	{
		this.startingPoint = startingPoint;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("Track [\n\tid=");
		builder.append(id);
		builder.append("\n\tname=");
		builder.append(name);
		builder.append("\n\tpieces=");
		builder.append(pieces);
		builder.append("\n\tlanes=");
		builder.append(lanes);
		builder.append("\n\tstartingPoint=");
		builder.append(startingPoint);
		builder.append("\n\t]");
		return builder.toString();
	}

}