package track;

import com.google.gson.annotations.SerializedName;

public class Piece
{

	private double length;

	@SerializedName("switch")
	private boolean isSwitch;
	private boolean bridge;
	private int radius;
	private double angle;

	public double getAngle()
	{
		return angle;
	}

	public double getLength()
	{
		return length;
	}

	public int getRadius()
	{
		return radius;
	}

	public boolean isBridge()
	{
		return bridge;
	}

	public boolean isSwitch()
	{
		return isSwitch;
	}

	public void setAngle(double angle)
	{
		this.angle = angle;
	}

	public void setBridge(boolean bridge)
	{
		this.bridge = bridge;
	}

	public void setLength(double length)
	{
		this.length = length;
	}

	public void setRadius(int radius)
	{
		this.radius = radius;
	}

	public void setSwitch(boolean isSwitch)
	{
		this.isSwitch = isSwitch;
	}

	@Override
	public String toString()
	{
		return "Piece [length=" + length + ", isSwitch=" + isSwitch + ", bridge=" + bridge + ", radius=" + radius
				+ ", angle=" + angle + "]";
	}

}
