package track;

public class PiecePosition
{
	private int pieceIndex;
	private double inPieceDistance;
	private LanePosition lane;
	private int lap;
	public int getPieceIndex()
	{
		return pieceIndex;
	}
	public void setPieceIndex(int pieceIndex)
	{
		this.pieceIndex = pieceIndex;
	}
	public double getInPieceDistance()
	{
		return inPieceDistance;
	}
	public void setInPieceDistance(double inPieceDistance)
	{
		this.inPieceDistance = inPieceDistance;
	}
	public LanePosition getLane()
	{
		return lane;
	}
	public void setLane(LanePosition lane)
	{
		this.lane = lane;
	}
	public int getLap()
	{
		return lap;
	}
	public void setLap(int lap)
	{
		this.lap = lap;
	}
	@Override
	public String toString()
	{
		return "PiecePosition [pieceIndex=" + pieceIndex + ", inPieceDistance=" + inPieceDistance + ", lane=" + lane
				+ ", lap=" + lap + "]";
	}
	
	
	
}
