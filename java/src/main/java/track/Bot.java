package track;

public class Bot
{

	private String name;
	private String key;

	public Bot(String name, String key)
	{
		this.name = name;
		this.key = key;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("Bot [name=");
		builder.append(name);
		builder.append(", key=");
		builder.append(key);
		builder.append("]");
		return builder.toString();
	}

}
