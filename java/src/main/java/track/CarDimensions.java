package track;

public class CarDimensions
{

	private double length;
	private double width;
	private double guideFlagPosistion;

	public double getGuideFlagPosistion()
	{
		return guideFlagPosistion;
	}

	public double getLength()
	{
		return length;
	}

	public double getWidth()
	{
		return width;
	}

	public void setGuideFlagPosistion(double guideFlagPosistion)
	{
		this.guideFlagPosistion = guideFlagPosistion;
	}

	public void setLength(double length)
	{
		this.length = length;
	}

	public void setWidth(double width)
	{
		this.width = width;
	}

	@Override
	public String toString()
	{
		return "CarDimensions [length=" + length + ", width=" + width + ", guideFlagPosistion=" + guideFlagPosistion
				+ "]";
	}

}
