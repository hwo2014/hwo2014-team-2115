package track;

import java.awt.geom.Point2D;

public class StartingPoint
{
	private Point2D.Double position;
	private double angle;

	public double getAngle()
	{
		return angle;
	}

	public Point2D.Double getPosition()
	{
		return position;
	}

	public void setAngle(double angle)
	{
		this.angle = angle;
	}

	public void setPosition(Point2D.Double position)
	{
		this.position = position;
	}

	@Override
	public String toString()
	{
		return "StartingPoint [position=" + position + ", angle=" + angle + "]";
	}

}
