package track;

public class Lane
{
	private int index;
	private int distanceFromCenter;

	public int getDistanceFromCenter()
	{
		return distanceFromCenter;
	}

	public int getIndex()
	{
		return index;
	}

	public void setDistanceFromCenter(int distanceFromCenter)
	{
		this.distanceFromCenter = distanceFromCenter;
	}

	public void setIndex(int index)
	{
		this.index = index;
	}

	@Override
	public String toString()
	{
		return "Lane [index=" + index + ", distanceFromCenter=" + distanceFromCenter + "]";
	}

}
