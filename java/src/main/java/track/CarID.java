package track;

public class CarID
{
	private String name;
	private String color;

	public String getColor()
	{
		return color;
	}

	public String getName()
	{
		return name;
	}

	public void setColor(String color)
	{
		this.color = color;
	}

	public void setName(String name)
	{
		this.name = name;
	}
	
	public CarID(){
		
	}
	
	public CarID(String name, String color) {
		this.name = name;
		this.color = color;
	}
	
	

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		CarID other = (CarID) obj;
		if (color == null) {
			if (other.color != null) return false;
		}
		else if (!color.equals(other.color)) return false;
		if (name == null) {
			if (other.name != null) return false;
		}
		else if (!name.equals(other.name)) return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "CarID [name=" + name + ", color=" + color + "]";
	}

}
