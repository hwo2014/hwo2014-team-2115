package track;

public class RaceSession
{

	private int laps;
	private int maxLapTimeMs;
	private boolean quickRace;

	public int getLaps()
	{
		return laps;
	}

	public int getMaxLapTimeMs()
	{
		return maxLapTimeMs;
	}

	public boolean isQuickRace()
	{
		return quickRace;
	}

	public void setLaps(int laps)
	{
		this.laps = laps;
	}

	public void setMaxLapTimeMs(int maxLapTimeMs)
	{
		this.maxLapTimeMs = maxLapTimeMs;
	}

	public void setQuickRace(boolean quickRace)
	{
		this.quickRace = quickRace;
	}

	@Override
	public String toString()
	{
		return "RaceSession [laps=" + laps + ", maxLapTimeMs=" + maxLapTimeMs + ", quickRace=" + quickRace + "]";
	}

}
