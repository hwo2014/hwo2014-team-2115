package track;

public class Position
{
	private CarID id;
	private double angle;
	private PiecePosition piecePosition;
	public CarID getId()
	{
		return id;
	}
	public void setId(CarID id)
	{
		this.id = id;
	}
	public double getAngle()
	{
		return angle;
	}
	public void setAngle(double angle)
	{
		this.angle = angle;
	}
	public PiecePosition getPiecePosition()
	{
		return piecePosition;
	}
	public void setPiecePosition(PiecePosition piecePosition)
	{
		this.piecePosition = piecePosition;
	}
	@Override
	public String toString()
	{
		return "Position [id=" + id + ", angle=" + angle + ", piecePosition=" + piecePosition + "]";
	}
	
	
	
	
	
}
